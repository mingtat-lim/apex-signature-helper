// @ts-check

'use strict';

const crypto = require('crypto');
const util = require('util');
const _ = require('lodash');

// npm i https://github.com/GovTechSG/node-apex-api-security.git --save
const apexSecurityHelper = require('node-apex-api-security').ApiSigningUtil;

/**
 * 
 * @typedef {object} AuthParam
 * @property {string} authPrefix
 * @property {string} signatureMethod
 * @property {string} secret
 * @property {string} appId
 * @property {string} urlPath
 * @property {string} httpMethod
 * @property {object} queryString
 * @property {object} formData
 * @property {string} nonce
 * @property {string} timestamp
 * @property {string} baseString
 */

/**
 *  Show the baseString 
 *
 * @param { AuthParam } param
 */
function showBaseString(param) {
    // get baseString
    const baseProps = {
        authPrefix: param.authPrefix.toLowerCase(),
        // signatureMethod: param.signatureMethod || "signatureMethod",
        signatureMethod: param.signatureMethod || _.isNil(param.secret) ? 'SHA256withRSA' : 'HMACSHA256',
        appId: param.appId,
        urlPath: param.urlPath,
        httpMethod: param.httpMethod,
        queryString: param.queryString || null,
        formData: param.formData || null,
        nonce: param.nonce || 'nonce',
        timestamp: param.timestamp || 'timestamp',
    };

    param.baseString = apexSecurityHelper.getSignatureBaseString(baseProps);
}

/**
 * Funcion to return nonce
 * 
 * @returns string
 */
function nonceLib() {
    return crypto.randomBytes(32).toString('base64');
}

const SUPPORTED_HTTP_METHODS = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'];
const SUPPORTED_AUTH_PREFIX = ['apex_l1_eg', 'apex_l2_eg', 'apex_l1_ig', 'apex_l2_ig'];

const PARAMETER_MISSING = 'Required parameter is missing: --%s';
const REQUIRED_ONE_ERROR = 'Parameter is missing, expecting one of the following:\n  %s';
const INVALID_VALUE = 'Invalid parameter value: %s for %s \n  expecting one of the following:\n  %s';

const GENERATE_REQUIRED_PARAMETERS = ['httpMethod', 'urlPath', 'realm', 'authPrefix', 'appId'];
const GENERATE_REQUIRED_ONE_PARAMETER = ['secret', 'keyFile', 'keyString'];
const GENERATE_REQUIRED_VALUES = [{name: 'httpMethod', value: SUPPORTED_HTTP_METHODS}, {name: 'authPrefix', value: SUPPORTED_AUTH_PREFIX}];

const VALIDATE_REQUIRED_PARAMETERS = ['urlPath', 'httpMethod', 'authorizationToken'];
const VALIDATE_REQUIRED_ONE_PARAMETER = ['secret', 'keyFile', 'keyString'];
const VALIDATE_REQUIRED_VALUES = [{name: 'httpMethod', value: SUPPORTED_HTTP_METHODS}];


// const ERROR_MESSAGE_FORMAT = "%s \nInput Parameters:\n%s"

// var testExecution = false;

function validateMissingParameters(paramList, param) {
    let errorMessages = '';
    let errorCount = 0;

    paramList.forEach((paramName) => {
        const message = (param[paramName] == undefined || param[paramName] == null) ? util.format(PARAMETER_MISSING, paramName) : undefined;

        if (message != undefined) {
            if (errorCount == 0) {
                errorMessages = util.format('%s. %s', ++errorCount, message);
            } else {
                errorMessages = util.format('%s\n%s. %s', errorMessages, ++errorCount, message);
            }
        }
    });

    if (errorMessages.length > 0) {
        // throw new Error(util.format(ERROR_MESSAGE_FORMAT, errorMessages, JSON.stringify(param, null, 4)));
        throw new Error(errorMessages);
    }
}

function validateRequiredOne(paramList, param) {
    let paramFound = false;

    // console.log (JSON.stringify(param, null, 4))
    paramList.forEach((paramName) => {
        // console.log(">>>" + paramName + ">>>" + paramFound)
        paramFound = (param[paramName] == undefined || param[paramName] == null) ? (paramFound || false) : true;
        // console.log(">>>paramFound::>>>" + paramFound)
    });

    // console.log(">>>paramFound::" + paramFound)
    if (!paramFound) {
        const errorMessages = util.format(REQUIRED_ONE_ERROR, paramList);
        // throw new Error(util.format(ERROR_MESSAGE_FORMAT, errorMessages, JSON.stringify(param, null, 4)));
        throw new Error(errorMessages);
    }
}

function isOneExists(paramList, param) {
    let paramFound = false;

    // console.log (JSON.stringify(param, null, 4))
    paramList.forEach((paramName) => {
        // console.log(">>>" + paramName + ">>>" + paramFound)
        // paramFound = (param[paramName] == undefined || param[paramName] == null) ? (paramFound || false) : true;
        paramFound = paramFound || _.has(param, paramName);
        // console.log(">>>paramFound::>>>" + paramFound + ">>" + param[paramName] + "<<")
    });

    return paramFound;
}

function validateRequiredValues(paramList, param) {
    let errorMessages = '';
    let errorCount = 0;

    paramList.forEach((nameValuePair) => {
        let isError = true;
        const paramValue = param[nameValuePair.name].toUpperCase();

        nameValuePair.value.forEach((element) => {
            if (paramValue == element.toUpperCase()) isError = false;
        });

        if (isError) {
            const message = util.format(INVALID_VALUE, param[nameValuePair.name], nameValuePair.name, nameValuePair.value);

            if (errorCount == 0) {
                errorMessages = util.format('%s. %s', ++errorCount, message);
            } else {
                errorMessages = util.format('%s\n%s. %s', errorMessages, ++errorCount, message);
            }
        }
    });

    if (errorMessages.length > 0) {
        // throw new Error(util.format(ERROR_MESSAGE_FORMAT, errorMessages, JSON.stringify(param, null, 4)));
        throw new Error(errorMessages);
    }
}


function getApexSecurityToken(param) {
    // console.log("get getApexSecurityToken")
    // console.log(param)
    // console.log(isOneExists(GENERATE_REQUIRED_PARAMETERS, param))
    // no geteway security require...
    if (isOneExists(GENERATE_REQUIRED_PARAMETERS, param)) {
        // validate input parameters
        validateMissingParameters(GENERATE_REQUIRED_PARAMETERS, param);
        validateRequiredValues(GENERATE_REQUIRED_VALUES, param);
        validateRequiredOne(GENERATE_REQUIRED_ONE_PARAMETER, param);

        // patch, fields rename
        // param.urlPath = param.signatureUrl;
        // param.certFileName = param.privateCertFileName;

        // set nouce & timestamp
        param.nonce = param.nonce || nonceLib();
        param.timestamp = param.timestamp || (new Date).getTime();

        // set signatureMethod
        // param.signatureMethod = _.isNil(param.secret) ? 'SHA256withRSA' : 'HMACSHA256';

        // console.log("get signature")
        param.signature = apexSecurityHelper.getSignatureToken(param);

        if (param.debug) showBaseString(param);
    }

    // if (param.nextHop != undefined && param.nextHop.signatureUrl != undefined) {
    if (param.nextHop !== undefined && isOneExists(GENERATE_REQUIRED_PARAMETERS, param.nextHop)) {
        // validate input parameters
        validateMissingParameters(GENERATE_REQUIRED_PARAMETERS, param.nextHop);
        validateRequiredValues(GENERATE_REQUIRED_VALUES, param.nextHop);
        validateRequiredOne(GENERATE_REQUIRED_ONE_PARAMETER, param.nextHop);

        // propagate queryString and formData to nextHop...
        param.nextHop.queryString = param.queryString;
        param.nextHop.formData = param.formData;

        // propagate old peroperty...
        // param.nextHop.urlPath = param.nextHop.signatureUrl;
        // param.nextHop.certFileName = param.nextHop.privateCertFileName;

        // set nouce & timestamp
        param.nextHop.nonce = param.nextHop.nonce || nonceLib();
        param.nextHop.timestamp = param.nextHop.timestamp || (new Date).getTime();

        // set signatureMethod
        // param.nextHop.signatureMethod = _.isNil(param.secret) ? 'SHA256withRSA' : 'HMACSHA256';

        const childToken = apexSecurityHelper.getSignatureToken(param.nextHop);

        if (param.debug) showBaseString(param.nextHop);

        if (childToken != null) {
            if (param.signature != undefined && param.signature.length > 0 ) {
                param.signature += ', ' + childToken;
            } else {
                param.signature = childToken;
            }
        }
    }

    return param;
}

/*
try {
    var securityToken = getApexSecurityToken({ "signatureUrl": "empty" })
} catch(error) {
    console.log("Test Case 1.\n" + error.message);
}

try {
    var securityToken = getApexSecurityToken({
        httpMethod: "GETx",
        signatureUrl: "https://www.example.com",

        realm: "www.sample.com",
        authPrefix: "apex_l1_egx",
        appId: "my-app",
        secret: "my-secret"
    })

    console.log(securityToken);
} catch(error) {
    console.log("Test Case 2.\n" + error.message);
}


try {
    var securityToken = getApexSecurityToken({
        httpMethod: "GET",
        signatureUrl: "https://www.example.com",

        realm: "www.sample.com",
        authPrefix: "apex_l1_eg",
        appId: "my-app",
        secret: "my-secret",

        nextHop: {
            signatureUrl: "https://www.example.com"
        }
    })

    console.log(securityToken);
} catch(error) {
    console.log("Test Case 3.\n" + error.message);
}


try {
    var securityToken = getApexSecurityToken({
        httpMethod: "GET",
        signatureUrl: "https://www.example.com",

        realm: "www.sample.com",
        authPrefix: "apex_l1_eg",
        appId: "my-app",
        secret: "my-secret",

        queryString: { param1: "value1", param2: "value2"},
        formData: { formParam1: "formValue1", formParam2: "formValue2"},

        nextHop: {
            httpMethod: "GET",
            signatureUrl: "https://www.example.com",

            realm: "www.sample.com",
            authPrefix: "apex_l1_eg",
            appId: "my-app",
            keyFile: "nd_awsplayZ.key",
            passphrase: "password"
            }
    })

    console.log(securityToken);
} catch(error) {
    console.log("Test Case 4.\n" + error.message);
}
*/

function getApexSecurityParameters(param, skipValidation) {
    // console.log(param)
    // console.log("===============")
    if (!(skipValidation === true)) {
        validateMissingParameters(VALIDATE_REQUIRED_PARAMETERS, param);
        validateRequiredValues(VALIDATE_REQUIRED_VALUES, param);
        validateRequiredOne(VALIDATE_REQUIRED_ONE_PARAMETER, param);
    }

    // const regex = /apex_l(1|2)_(i|e)g /gmi;
    // const regex = /(apex_l1_eg |apex_l2_eg |apex_l1_ig |apex_l2_ig )/gi;
    const regex = /apex_l1_eg |apex_l2_eg |apex_l1_ig |apex_l2_ig /gi;
    // const regex = /apex_l(1|2)/gmi;

    const authPrefixs = [];

    let m;
    while (( m = regex.exec(param.authorizationToken)) !== null) {
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        m.forEach((match, groupIndex) => {
            // console.log(`Found match, group ${groupIndex}: ${match}`)
            authPrefixs.push(match.trim());
        });
    }
    // console.log("found::" + authPrefixs)

    const nameValuePair = [];
    param.authorizationToken.split(',').forEach((element) => {
        const nameValue = element.split('=');
        nameValuePair.push({
            'name': nameValue[0].trim(),
            'value': nameValue.length == 1 ? undefined : nameValue.slice(1).join('=').replace(/"/g, ''),
        });
    });

    // console.log(nameValuePair)
    const results = [];

    authPrefixs.forEach((authPrefix) => {
        const result = {
            'authPrefix': authPrefix,
        };
        _.merge(result, param);

        nameValuePair.forEach((nameValue) => {
            // var name = nameValue.name.replace(authPrefix.toLowerCase(), 'xxx')
            const regex = new RegExp(authPrefix.trim(), 'gi');
            const name = nameValue.name.replace(regex, 'selected');
            // console.log(`${authPrefix}::${nameValue.name}: ${name}`)
            switch (name) {
            case 'selected realm':
                result['realm'] = nameValue.value;
                break;
            case 'selected_app_id':
                result['appId'] = nameValue.value;
                break;
            case 'selected_nonce':
                result['nonce'] = nameValue.value;
                break;
            case 'selected_signature_method':
                result['signatureMethod'] = nameValue.value;
                break;
            case 'selected_timestamp':
                result['timestamp'] = nameValue.value;
                break;
            case 'selected_version':
                result['version'] = nameValue.value;
                break;
            case 'selected_signature':
                result['originalSignature'] = nameValue.value;
                break;
            }
        });

        results.push(result);
    });

    // console.log(results)
    return results;
}


module.exports = {
    getApexSecurityToken: getApexSecurityToken,
    getApexSecurityParameters: getApexSecurityParameters,
    nonce: nonceLib,
};

