'use strict';

var isMacOS = process.platform === 'darwin';

const _ = require('lodash');
//const chalk = require('chalk');
const chalk_actual = require('chalk');

var chalk = {
    red(message) {
        if (isMacOS) return chalk_actual.red(message);
        else return message;
    },

    yellow(message) {
        if (isMacOS) return chalk_actual.yellow(message);
        else return message;
    },

    blue(message) {
        if (isMacOS) return chalk_actual.blue(message);
        else return message;
    },

    green(message) {
        if (isMacOS) return chalk_actual.green(message);
        else return message;
    },
};

const util = require('util');

const { getApexSecurityToken, getApexSecurityParameters } = require('./lib/main.js');

const version = require('./package.json').version;

const minimistOptions = {
    string: [
        'nonce',
        'timestamp',
        'keyFile',
        'keyString',
        'passphrase',
        'jsonFile',
        'secret',
        'authorizationToken',
        'queryString',
        'formData',
    ],
    number: ['timestamp'],
    boolean: ['debug', 'version', 'help'],
    default: { realm: 'www.test.gov.sg' },
    alias: {
        a: 'appId',
        s: 'secret',
        u: 'urlPath',
        p: 'authPrefix',
        m: 'httpMethod',

        x: 'authorizationToken',
        q: 'queryString',
        f: 'formData',

        n: 'nonce',
        t: 'timestamp',

        j: 'jsonFile',

        h: 'help',
        v: 'version',

        d: 'debug', // display the debug information
    },
};

// convert the paremeters into JSON
var argv = require('minimist')(process.argv.slice(2), minimistOptions);

//console.log(argv)

// remove alias properties from argv
//delete argv['_']
_.forIn(minimistOptions.alias, function (value, key) {
    //console.log(key);
    delete argv[key];
});

try {
    // convert string to json
    if (!_.isNil(argv.queryString)) argv.queryString = JSON.parse(argv.queryString);
    if (!_.isNil(argv.formData)) argv.formData = JSON.parse(argv.formData);
} catch (error) {
    var errorMessage = error.message;
    //errorMessage = errorMessage.replace(/^Cannot find module/, "Cannot find data file")

    if (error.message == 'Unexpected end of JSON input') {
        console.log(chalk.red('Error: ' + errorMessage + ' for queryString or formData'));
        console.log();

        console.log(chalk.yellow('i.e. -q or --queryString \'{ "data": "my data" }\''));
        console.log(chalk.yellow('i.e. -f or --formData \'{ "data": "my data" }\''));
        console.log();
    }
    return;
}

//console.dir(argv);
//     if (Array.isArray(params)){
var params;
if (argv.jsonFile != undefined && argv.jsonFile != null) {
    //params = require(argv.jsonFile)
    try {
        var filePath = argv.jsonFile;

        // on macOS, remove ./ and / from parameter jsonFile
        if (isMacOS) filePath = filePath.replace(/^\.\/|^\//, '');
        //console.log(filePath)

        params = require(process.cwd() + '/' + filePath);
    } catch (error) {
        var errorMessage = error.message;
        errorMessage = errorMessage.replace(/^Cannot find module/, 'Cannot find data file');

        console.log(chalk.red(errorMessage));
        return;
    }

    if (!Array.isArray(params)) {
        params = [params];
    }
} else {
    params = [argv];
}

if (argv._.length > 0) {
    //console.log(argv)
    //console.log(params)
    //return;

    switch (argv._[0].toLowerCase()) {
        case 'gen':
        case 'generate':
            generateAuthorizationToken(params);
            break;
        case 'val':
        case 'validate':
            //console.log(params)
            //console.log("+++++++++++++")
            verifyAuthorizationToken(params);
            break;
        case 'help':
            commamnd_help();
            commamnd_helpDetails();
            command_parametersHelp();
            break;
        default:
            //console.log("default")
            error_invalidCommand();
        //const siteUrl = new URL("https://www.example.com:8080/test/v1");
        //console.log(siteUrl)
    }
} else {
    if (argv.help) {
        commamnd_help();
        commamnd_helpDetails();
        command_parametersHelp();
    } else if (argv.version) {
        console.log(chalk.yellow('ash version: ' + version));
    } else {
        //console.log("else")
        error_invalidCommand();
    }
}

function error_invalidCommand() {
    console.log(chalk.red('Error: Invalid or missing command.'));
    commamnd_help();
}

function commamnd_help() {
    console.log();
    console.log(chalk.yellow(`APEX Signature Helper. v${version}\n`));
    console.log(chalk.yellow('usage: ash <command> [--flag] [-f]\n'));
    console.log(chalk.yellow('where <command> is one of:\n    gen, generate, val, validate, help'));
    console.log();
}

function commamnd_helpDetails() {
    console.log(chalk.yellow('  gen, generate       Generate APEX Signature'));
    console.log(chalk.yellow('  val, validate       Validate APEX Signature'));
    console.log(chalk.yellow(' help, help           Display details help'));
    console.log();
}

function command_parametersHelp() {
    console.log(chalk.yellow('where --flag is one or more of the following:'));
    console.log();

    console.log(chalk.yellow('  -m, --httpMethod           HTTP Method'));
    console.log(chalk.yellow('  -u, --urlPath              URL Path'));
    console.log(chalk.yellow('  -p, --authPrefix           APEX Authorization Prefix'));
    console.log(chalk.yellow('  -a, --appId                APEX App Id'));
    console.log(chalk.yellow('  -s, --secret               Apex App Secret'));
    console.log(chalk.yellow('      --keyFile              PEM File Name'));
    console.log(chalk.yellow('      --keyString            PEM String'));
    console.log(chalk.yellow('      --passphrase           Pass Phrase for PEM File'));

    console.log(chalk.yellow('  -n, --nonce                Nonce for L1/L2'));
    console.log(chalk.yellow('  -t, --timestamp            Timestamp for L1/L2'));
    console.log(chalk.yellow('  -q, --queryString          QueryString in JSON format'));
    console.log(chalk.yellow('  -f, --formData             Form Data in JSON format'));

    console.log(chalk.yellow('  -x, --authorizationToken   APEX authorization token'));

    console.log(
        chalk.yellow(
            '  -j, --jsonFile             JSON File for parameters, may contains one or array of JSON definition'
        )
    );

    console.log();
}

function generateAuthorizationToken(params) {
    params.forEach((param) => {
        _.merge(param, argv);

        //console.log(param)
        console.log();
        try {
            var securityToken = getApexSecurityToken(param);

            if (securityToken.signature != undefined) {
                var desc = securityToken.description == undefined ? 'Authorization Token:' : securityToken.description;

                console.log(chalk.blue(desc));
                console.log(chalk.green(securityToken.signature));

                if (securityToken.debug) {
                    console.log();
                    console.log(chalk.blue('BaseString'));
                    console.log(chalk.green(securityToken.baseString));

                    console.log();
                    console.log(chalk.blue('Input Parameters'));
                    //console.log(JSON.stringify(securityToken, null, 4))
                    console.log(securityToken);
                }
            } else console.log(chalk.red('Authorization token not generated, no L1/L2 properties found.'));
        } catch (error) {
            console.log(chalk.blue('Error:'));
            console.log(chalk.red(error.message));
            console.log();
            console.log(chalk.blue('Input Parameters:'));
            //console.log(chalk.yellow(JSON.parse(JSON.stringify(param))))
            //console.log(chalk.yellow(JSON.stringify(param, null, 4)))
            console.log(param);
        }
    });
    console.log();
}

function verifyAuthorizationToken(params) {
    params.forEach((param) => {
        _.merge(param, argv);

        console.log();
        try {
            // convert authorizationToken into json object
            var results = getApexSecurityParameters(param);

            //generateAuthorizationToken(result)
            //console.log("=====")
            //console.log(results.length)
            if (results.length === 0) {
                console.log(chalk.blue('Error:'));
                console.log(chalk.red('Unable to locate any valid APEX L1/L2 headers from authorizationToken.'));

                console.log();
                console.log(chalk.blue('Input Parameters:'));
                console.log(param);
            }
            //console.log(">>>>>")
            results.forEach((result) => {
                // get the new authorizationToken from json object
                var securityToken = getApexSecurityToken(result);

                // extract the signature from new authorizationToken
                var signature = getApexSecurityParameters({ authorizationToken: securityToken.signature }, true);

                // save the signature
                securityToken.signature = signature[0].originalSignature;

                securityToken.signatureIsValid =
                    securityToken.signature === securityToken.originalSignature ? true : false;

                var desc =
                    securityToken.description == undefined ? 'Signature Verification:' : securityToken.description;
                if (securityToken.signatureIsValid) {
                    console.log(chalk.blue(desc));
                    console.log(chalk.green('Successful...'));
                    console.log('Signature: ' + chalk.green(securityToken.signature));

                    if (securityToken.debug) {
                        console.log();
                        console.log(chalk.blue('Input Parameters:'));
                        console.log(securityToken);
                    }
                } else {
                    console.log(chalk.blue(desc));
                    console.log(chalk.red('Signature verification failed.'));

                    console.log();
                    console.log(chalk.blue('Input Parameters:'));
                    console.log(securityToken);
                }
                //console.log(securityToken)
            });
        } catch (error) {
            console.log(chalk.blue('Error:'));
            console.log(chalk.red(error.message));
            console.log();
            console.log(chalk.blue('Input Parameters:'));
            //console.log(chalk.yellow(JSON.parse(JSON.stringify(param))))
            //console.log(chalk.yellow(JSON.stringify(param, null, 4)))
            console.log(param);
        }
    });
    console.log();
}
