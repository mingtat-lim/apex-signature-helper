// npm i --save-dev chai
// var assert = require('assert');
// const assert = require('chai').assert;
const expect = require('chai').expect;

const rewire = require('rewire');
// software under test
const sut = rewire('../lib/main.js'); // Bring your module in with rewire

const getApexSecurityToken = sut.__get__('getApexSecurityToken');
const getApexSecurityParameters = sut.__get__('getApexSecurityParameters');

function validateRequiredParameters(param, expected) {
    expect(function() {
        getApexSecurityToken(param);
    }).to.throw(Error, expected);

    expect(function() {
        getApexSecurityToken({'nextHop': param});
    }).to.throw(Error, expected);
}

describe('Authorization Token Parameters Validation', function() {
    context('Required parameter is missing', function() {
        it('when urlPath is missing, should throw error', function() {
            const param = {'realm': 'empty', 'httpMethod': '', 'authPrefix': '', 'appId': ''};
            const expected = '1. Required parameter is missing: --urlPath';

            validateRequiredParameters(param, expected);
        });

        it('when httpMethod is missing, should throw error', function() {
            const param = {'urlPath': 'empty', 'realm': '', 'authPrefix': '', 'appId': ''};
            const expected = '1. Required parameter is missing: --httpMethod';

            validateRequiredParameters(param, expected);
        });

        it('when realm is missing, should throw error', function() {
            const param = {'urlPath': 'empty', 'httpMethod': '', 'authPrefix': '', 'appId': ''};
            const expected = '1. Required parameter is missing: --realm';

            validateRequiredParameters(param, expected);
        });

        it('when authPrefix is missing, should throw error', function() {
            const param = {'urlPath': 'empty', 'httpMethod': '', 'realm': '', 'appId': ''};
            const expected = '1. Required parameter is missing: --authPrefix';

            validateRequiredParameters(param, expected);
        });

        it('when appId is missing, should throw error', function() {
            const param = {'urlPath': 'empty', 'httpMethod': '', 'authPrefix': '', 'realm': ''};
            const expected = '1. Required parameter is missing: --appId';

            validateRequiredParameters(param, expected);
        });
    });

    context('Parameter has invalid value', function() {
        it('when httpMethod has invalid parameter value', function() {
            const param = {'urlPath': '', 'realm': '', 'httpMethod': 'TEST', 'authPrefix': 'apex_l1_eg', 'appId': ''};
            const expected = '1. Invalid parameter value: TEST for httpMethod';

            validateRequiredParameters(param, expected);
        });

        it('when authPrefix has invalid parameter value', function() {
            const param = {'urlPath': '', 'realm': '', 'httpMethod': 'GET', 'authPrefix': 'TEST', 'appId': ''};
            const expected = '1. Invalid parameter value: TEST for authPrefix';

            validateRequiredParameters(param, expected);
        });
    });

    context('Optional parameters is missing', function() {
        it('when secret, keyFile and keyString value is missing', function() {
            const param = {'urlPath': '', 'realm': '', 'httpMethod': 'GET', 'authPrefix': 'apex_l1_eg', 'appId': ''};
            const expected = 'Parameter is missing, expecting one of the following:';

            validateRequiredParameters(param, expected);
        });
    });
});


function validateRequiredParameters2(param, expected) {
    expect(function() {
        getApexSecurityParameters(param);
    }).to.throw(Error, expected);
}

describe('Verify Authorization Token Parameters Validation', function() {
    context('Required parameter is missing', function() {
        it('when urlPath is missing, should throw error', function() {
            const param = {httpMethod: 'GET', authorizationToken: 'apex_l1_eg'};
            const expected = '1. Required parameter is missing: --urlPath';

            validateRequiredParameters2(param, expected);
        });

        it('when httpMethod is missing, should throw error', function() {
            const param = {urlPath: 'https://www.example.com', authorizationToken: 'apex_l1_eg'};
            const expected = '1. Required parameter is missing: --httpMethod';

            validateRequiredParameters2(param, expected);
        });
    });
});

